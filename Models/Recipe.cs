﻿namespace RecipeHeaven.Models
{
    public class Recipe
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string? ImageName { get; set; }

        public List<RecipeIngredient> RecipeIngredients { get; set; }

        public List<PrepStep> PrepSteps { get; set; }
    }
}
