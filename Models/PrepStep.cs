﻿namespace RecipeHeaven.Models
{
    public class PrepStep
    {
        public int Id { get; set; }

        public string StepDescription { get; set; }

        public Recipe Recipe { get; set; }

        public int RecipeId { get; set; }
    }
}
