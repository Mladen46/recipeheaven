﻿namespace RecipeHeaven.Data.DTO
{
    public class RecipeDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string? ImageName { get; set; }

        public List<IngredientDTO> Ingredients { get; set; }

        public List<PrepStepDTO> PrepSteps { get; set; }
    }
}
