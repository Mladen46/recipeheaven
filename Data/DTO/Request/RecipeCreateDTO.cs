﻿namespace RecipeHeaven.Data.DTO.Request
{
    public class RecipeCreateDTO
    {
        public string Name { get; set; }

        public string? Description { get; set; }

        public List<IngredientDTO> IngredientDtos { get; set; }

        public List<PrepStepDTO> PrepStepDtos { get; set; }
    }
}
