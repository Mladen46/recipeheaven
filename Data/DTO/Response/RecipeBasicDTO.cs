﻿namespace RecipeHeaven.Data.DTO.Response
{
    public class RecipeBasicDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public string? ImageName { get; set; }
    }
}
