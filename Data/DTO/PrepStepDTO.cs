﻿namespace RecipeHeaven.Data.DTO
{
    public class PrepStepDTO
    {
        public int Id { get; set; }

        public string StepDescription { get; set; }
    }
}
