﻿namespace RecipeHeaven.Data
{
    using Microsoft.EntityFrameworkCore;
    using RecipeHeaven.Models;

    public class RecipeHeavenDbContext : DbContext
    {
        public RecipeHeavenDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RecipeIngredient>().HasKey(ri => new
            {
                ri.RecipeId,
                ri.IngredientId,
            });

            modelBuilder.Entity<RecipeIngredient>()
                .HasOne(ri => ri.Recipe)
                .WithMany(r => r.RecipeIngredients)
                .HasForeignKey(ri => ri.RecipeId);

            modelBuilder.Entity<RecipeIngredient>()
                .HasOne(ri => ri.Ingredient)
                .WithMany(i => i.RecipeIngredients)
                .HasForeignKey(ri => ri.IngredientId);

            modelBuilder.Entity<PrepStep>()
                .HasOne(ps => ps.Recipe)
                .WithMany(r => r.PrepSteps)
                .HasForeignKey(ps => ps.RecipeId);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Recipe> Recipes { get; set; }

        public DbSet<Ingredient> Ingredients { get; set; }

        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }

        public DbSet<PrepStep> PrepSteps { get; set; }
    }
}
