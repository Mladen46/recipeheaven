﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RecipeHeaven.Migrations
{
    /// <inheritdoc />
    public partial class RecipeHeavenV2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageName",
                table: "Recipes",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageName",
                table: "Recipes");
        }
    }
}
