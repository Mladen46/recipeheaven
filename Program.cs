using Microsoft.EntityFrameworkCore;
using RecipeHeaven.Data;
using RecipeHeaven.Interfaces;
using RecipeHeaven.Repositories;
using RecipeHeaven.Utility;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<RecipeHeavenDbContext>(optionsAction: options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnectionString")));
builder.Services.AddScoped<IRecipesRepository, RecipesRepository>();
builder.Services.AddScoped<DtoMapper, DtoMapper>();
builder.Services.AddCors(options =>
{
    options.AddPolicy("corspolicy", build =>
    {
        build.WithOrigins(builder.Configuration.GetValue<string>("AllowedHosts")!)
        .AllowAnyMethod()
        .AllowAnyHeader();
    });
});

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("corspolicy");

app.UseAuthorization();
app.UseStaticFiles();

app.MapControllers();

app.Run();
