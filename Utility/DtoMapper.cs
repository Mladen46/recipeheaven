﻿namespace RecipeHeaven.Utility
{
    using RecipeHeaven.Data.DTO;
    using RecipeHeaven.Data.DTO.Request;
    using RecipeHeaven.Data.DTO.Response;
    using RecipeHeaven.Models;

    public class DtoMapper
    {
        public RecipeBasicDTO ToRecipeBasicDto(Recipe recipe)
        {
            return new RecipeBasicDTO
            {
                Id = recipe.Id,
                Name = recipe.Name,
                Description = recipe.Description,
                ImageName = recipe.ImageName,
            };
        }


        public RecipeDTO ToRecipeDto(Recipe recipe)
        {
            return new RecipeDTO
            {
                Id = recipe.Id,
                Name = recipe.Name,
                Description = recipe.Description,
                ImageName = recipe.ImageName,
                Ingredients = recipe.RecipeIngredients
                    .Select(ri => this.ToIngredientDto(ri.Ingredient))
                    .ToList(),
                PrepSteps = recipe.PrepSteps
                    .Select(this.ToPrepStepDto)
                    .ToList(),
            };
        }

        public Recipe ToRecipe(RecipeCreateDTO recipeCreateDto)
        {
            return new Recipe
            {
                Name = recipeCreateDto.Name,
                Description = recipeCreateDto.Description,
                //ImageName = recipeCreateDto.Image != null ?
                //    recipeCreateDto.Image.FileName : null,
            };
        }

        public IngredientDTO ToIngredientDto(Ingredient ingredient)
        {
            return new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };
        }

        public Ingredient ToIngredient(IngredientDTO ingredientDto)
        {
            return new Ingredient
            {
                Id = ingredientDto.Id,
                Name = ingredientDto.Name,
            };
        }

        public PrepStepDTO ToPrepStepDto(PrepStep prepStep)
        {
            return new PrepStepDTO
            {
                Id = prepStep.Id,
                StepDescription = prepStep.StepDescription,
            };
        }

        public PrepStep ToPrepStep(PrepStepDTO prepStepDTO)
        {
            return new PrepStep
            {
                Id = prepStepDTO.Id,
                StepDescription = prepStepDTO.StepDescription,
            };
        }
    }
}
