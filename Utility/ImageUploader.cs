﻿namespace RecipeHeaven.Utility
{
    public static class ImageUploader
    {
        public static async Task<string> UploadImage(string path, IFormFile image)
        {
            if (image.FileName == null || image.FileName.Length == 0)
            {
                throw new Exception();
            }

            using (FileStream stream = new FileStream(path + image.FileName, FileMode.Create))
            {
                await image.CopyToAsync(stream);
                stream.Close();
            }

            return path + image.FileName;
        }
    }
}
