﻿namespace RecipeHeaven.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using RecipeHeaven.Data.DTO;
    using RecipeHeaven.Data.DTO.Request;
    using RecipeHeaven.Data.DTO.Response;
    using RecipeHeaven.Interfaces;
    using RecipeHeaven.Models;
    using RecipeHeaven.Utility;

    [ApiController]
    [Route("api/[controller]")]
    public class RecipesController : ControllerBase
    {
        private readonly IRecipesRepository recipesRepository;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly DtoMapper dtoMapper;

        public RecipesController(
            IRecipesRepository recipesRepository,
            IWebHostEnvironment webHostEnvironment,
            DtoMapper dtoMapper)
        {
            this.recipesRepository = recipesRepository;
            this.webHostEnvironment = webHostEnvironment;
            this.dtoMapper = dtoMapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<RecipeBasicDTO>))]
        public IActionResult GetAll()
        {
            return this.Ok(
                this.recipesRepository
                    .GetAll()
                    .Select(this.dtoMapper.ToRecipeBasicDto));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RecipeDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            if (this.recipesRepository.Get(id) == null)
            {
                return this.NotFound();
            }

            return this.Ok(
                this.dtoMapper.ToRecipeDto(this.recipesRepository.Get(id)!));
        }

        [HttpGet("Ingredients")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Ingredient>))]
        public IActionResult GetIngredients()
        {
            return this.Ok(this.recipesRepository.GetIngredients());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Put([FromBody] RecipeCreateDTO recipeCreateDto)
        {
            Recipe recipeCreate = this.dtoMapper.ToRecipe(recipeCreateDto);
            if (!this.recipesRepository.PutRecipe(
                recipeCreate,
                recipeCreateDto.IngredientDtos.Select(this.dtoMapper.ToIngredient).ToList(),
                recipeCreateDto.PrepStepDtos.Select(this.dtoMapper.ToPrepStep).ToList()))
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }

            return this.Ok(recipeCreate.Id);
        }

        [HttpPost("Image")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutRecipeInfo(
            [FromQuery] int recipeId,
            [FromForm] IFormFile image)
        {
            if (!this.recipesRepository.UpdateRecipeImage(recipeId, image.FileName))
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }

            await ImageUploader.UploadImage(
                path: this.webHostEnvironment.WebRootPath + "\\Api\\Images\\",
                image: image);

            return this.Ok();
        }
    }
}
