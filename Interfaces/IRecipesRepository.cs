﻿namespace RecipeHeaven.Interfaces
{
    using RecipeHeaven.Models;

    public interface IRecipesRepository
    {
        public List<Recipe> GetAll();

        public Recipe? Get(int id);

        public List<Ingredient> GetIngredients();

        public bool PutRecipe(
            Recipe recipe,
            List<Ingredient> ingredients,
            List<PrepStep> prepSteps);

        public bool UpdateRecipeImage(int recipeId, string imageName);
    }
}
