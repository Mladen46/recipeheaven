﻿namespace RecipeHeaven.Repositories
{
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;
    using RecipeHeaven.Data;
    using RecipeHeaven.Interfaces;
    using RecipeHeaven.Models;

    public class RecipesRepository : IRecipesRepository
    {
        private readonly RecipeHeavenDbContext context;

        public RecipesRepository(RecipeHeavenDbContext context)
        {
            this.context = context;
        }

        public List<Recipe> GetAll()
        {
            return this.context.Recipes
                .Include(r => r.RecipeIngredients)
                .ToList();
        }

        public Recipe? Get(int id)
        {
            return this.context.Recipes
                .Where(r => r.Id == id)
                .Include(r => r.RecipeIngredients)
                .ThenInclude(ri => ri.Ingredient)
                .Include(r => r.PrepSteps)
                .FirstOrDefault();
        }

        public List<Ingredient> GetIngredients()
        {
            return this.context.Ingredients.ToList();
        }

        public bool PutRecipe(
            Recipe recipe,
            List<Ingredient> ingredients,
            List<PrepStep> prepSteps)
        {
            IDbContextTransaction transaction =
                this.context.Database.BeginTransaction();
            try
            {
                this.context.Add(recipe);
                int rowCount = this.context.SaveChanges() +
                    this.PutInfoForRecipe(recipe.Id, ingredients, prepSteps);
                transaction.Commit();
                return rowCount > 0 ? true : false;
            }
            catch (Exception)
            {
                transaction.Rollback();
                return false;
            }
       }

        private int PutInfoForRecipe(
            int recipeId,
            List<Ingredient> ingredients,
            List<PrepStep> prepSteps)
        {
            foreach (PrepStep prepStep in prepSteps)
            {
                prepStep.RecipeId = recipeId;
                this.context.Add(prepStep);
            }

            foreach (Ingredient ingredient in ingredients)
            {
                this.context.Add(new RecipeIngredient
                {
                    RecipeId = recipeId,
                    IngredientId = ingredient.Id,
                });
            }

            return this.context.SaveChanges();
        }

        public bool UpdateRecipeImage(int recipeId, string imageName)
        {
            Recipe recipe = this.context.Recipes.Single(r => r.Id == recipeId);
            recipe.ImageName = imageName;
            return this.context.SaveChanges() > 0;
        }
    }
}
